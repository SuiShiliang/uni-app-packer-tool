uni-app 离线打包工具
# 
# 安装教程

1.  安装Python
2.  使用pip安装依赖包
3.  到对应Python文件下，直接run

# Python依赖安装及pycharm 配置
    pycharm 配置导出Python依赖
    方法一：
        在pycharm中打开命令行，主要当前目录必须是项目根目录
        输入：   cd .\venv\Scripts\
                .\activate.bat
        再输入： pip3.exe freeze > requirements.txt
        即可导出依赖文件 requirements.txt 到项目根目录下，
    方法二：
        点击pycharm的运行配置，即右三角形旁边的向下箭头
        选择 Edit Configurations...
        点击 + 号
        选择 Shell Script 
        在 Execute 中，选择 Script Text
        在 Script Text 中输入 pip3.exe freeze > requirements.txt
        保存
        再点击 运行，即可将依赖文件 requirements.txt 导出到项目根目录下，
    pycharm 配置安装Python依赖
    方法一：
        在pycharm中打开命令行，主要当前目录必须是项目根目录
        输入：    cd .\venv\Scripts\
                .\activate.bat
        再输入： pip3.exe install -r requirements.txt
        即可安装项目Python依赖
    方法二：
        点击pycharm的运行配置，即右三角形旁边的向下箭头
        选择 Edit Configurations...
        点击 + 号
        选择 Shell Script 
        在 Execute 中，选择 Script Text
        在 Script Text 中输入 pip3.exe install -r requirements.txt
        保存
        再点击 运行，即安装依赖文件 requirements.txt 对应的Python依赖

# 使用说明
1.  使用 pip install -r requirements.txt


# 证书配置问题
## 私有项目证书获取
### 自行生成
1. 安装jdk1.8_201版本
2. 输入命令： keytool -genkey -alias __UNI__7EDAFE9 -keyalg RSA -keysize 2048 -validity 36500 -keystore __UNI__7EDAFE9.keystore
```
-alias __UNI__7EDAFE9: __UNI__7EDAFE9为签名文件别名，可自定义，建议使用uniapp的appID
-validity 36500： 为签名文件有效期，单位为天
-keystore __UNI__7EDAFE9.keystore：__UNI__7EDAFE9.keystore为生成的签名文件名称
输入命令后，相关信息可随意填，但必须记住密码
```
3. 查看相关信息命令：keytool -list -v -keystore __UNI__7EDAFE9.keystore
```
-keystore __UNI__7EDAFE9.keystore: 为生成的签名文件名称
查看对应的：
    别名: hyx.engineer
    证书指纹:
         MD5:  18:3C:33:8A:50:12:65:61:B8:7E:D1:1C:69:6F:2C:8F
         SHA1: CE:72:79:DE:DD:11:BE:98:2C:46:3E:02:1B:25:58:F6:4B:F6:57:A9
         SHA256: 31:C6:48:C0:D6:7B:1C:55:F4:75:42:01:16:68:F6:53:4A:07:88:2C:63:D7:22:E9:0E:68:38:97:EE:79:23:40
```
4. 将对应的MD5等相关信息按照“uniapp生成”第4步填入

### uniapp生成
1. 网站：https://dev.dcloud.net.cn/pages/app/list
2. 点击应用名称
3. 点击Android云端证书，生成证书后，并下载下来
4. 点击各平台信息，点击修改，将证书的对应信息填入进入
5. 点击生成离线打包 key

# app系统权限配置问题
1. lib.5plus.base-release.aar形式的SDK已经包含如下feature模块：accelerometer audio barcode cache camera console device downloader  io gallery geolocation android nativeObj nativeUI navigator orientation proximity runtime storage share uploader webview net zip
2. 第三方开放平台如（高德地图定位，微信支付等）未包含在lib.5plus.base-release.aar中，各个模块具体配置参考SDK中Feature-Android.xls文件。
3. lib.5plus.base-release.aar因为部分权限涉及敏感权限，所以将权限全部移出aar，具体权限可参考AndroidManifest.xml进行配置。
4. 本sdk的使用许可，详见HBuilder X使用许可协议
5. 具体配置，请查看[Feature-Android.xls]的权限配置信息

# 打包工具参数配置
1. 在 [uniapp-offline-sdk](resource%2Funiapp-offline-sdk)resouce 目录下，创建版本号目录，复制uniapp离线sdk的HBuilder-Integrate-AS目录到版本号目录下
2. 在 [android-sdk](resource%2Fandroid-sdk%2Fandroid-sdk) 文件里添加对应的安卓sdk目录，建议通过Android studio进行下载安装安卓sdk
3. 在 [gradle-version](resource%2Fgradle-versions%2Fgradle-version) 文件里添加对应的gradle安装目录
4. 在 [hbuilderx-version](resource%2Fhbuilderx-versions%2Fhbuilderx-version) 文件里添加安装的HBuilderX的安装目录
5. 在 [java-version](resource%2Fjava-versions%2Fjava-version) 文件里添加安装的Java目录

# 注意事项
1. 进行打包时，需要启动HBuilderX，否则将无法打包UniApp离线资源，导致打包失败
   * 为什么要启动HBuilderX？因为HBuilderX的cli.exe会校验项目的归属问题，所有不启动并登录HBuilderX，打包会失败
2. HBuilderX的版本与UniApp的Android离线SDK版本必须一致，否则应用可能出现奇怪的问题
   * HBuilderX下载地址:  
     * 历史版本： https://hx.dcloud.net.cn/Tutorial/HistoryVersion
     * 最新版本：https://www.dcloud.io/hbuilderx.html
   * UniApp Android离线SDK地址地址：
     * 历史版本（也可打开最新版本下的历史版本）：https://pan.baidu.com/s/1KtOCtMZJSgfAayHNjTpdTg?pwd=4hvi
     * 最新版本：https://nativesupport.dcloud.net.cn/AppDocs/download/android.html
   * 配置文件为 [hbuilderx-version](resource%2Fhbuilderx-versions%2Fhbuilderx-version)
   * 格式为【 版本号 目录 】，例如: 【 4.08 D:\program file\HBuilderX 】
3. Gradle建议使用6.6版本
   * 配置文件为 [gradle-version](resource%2Fgradle-versions%2Fgradle-version)
   * 格式为【 版本号 目录 】，例如: 【 6.6 D:\program file\gradle-6.6 】
4. Java版本建议使用jdk1.8_201版本
   * 配置文件为 [java-version](resource%2Fjava-versions%2Fjava-version)
   * 格式为【 版本号 目录 】，例如: 【 oraclejdk-1.8.0_201 E:\BaiduNetdiskDownload\jdk1.8.0_201 】
5. Android SDK建议下载Android studio进行SDK管理，一般SDK安装目录在 C:\Users\10756\AppData\Local\Android\Sdk
   * 配置文件为 [android-sdk](resource%2Fandroid-sdk%2Fandroid-sdk)
   * 格式为【 安装目录 】，例如: 【 C:\Users\10756\AppData\Local\Android\Sdk 】
6. UniApp的Android离线SDK下的HBuilder-Integrate-AS复制到目录下的resource/uniapp-offline-sdk下
   1. 格式为 版本号/AS-project， 如 4.08/AS-project
7. 打包成功后文件地址，一般存于项目下的 project/package-project/项目
   * apk文件地址：project/package-project/项目/simpleDemo/build/outputs/apk/release下
   * UniApp本地资源库地址：project/package-project/项目/simpleDemo/src/main/assets/apps下
8. 