# coding:utf-8
import dataclasses
import datetime
import json
import os
import shutil
import subprocess
import sys
import tkinter as tk
import traceback
import xml.etree.ElementTree as ET
import zipfile
from tkinter import ttk, filedialog, simpledialog, messagebox
from tkinter.scrolledtext import ScrolledText
from tkinter.ttk import Combobox
from typing import Dict, Any, Callable
from queue import Queue
import threading


@dataclasses.dataclass
class ProjectSetting:
    project_name: str  # 项目名称
    project_save_name: str  # 保存记录名称
    project_path: str  # 项目路径
    android_sdk_home: str  # 安卓sdk目录
    java_version: str  # Java安装目录
    gradle_version: str  # Gradle安装目录
    hbuilderx_version: str  # HbuilderX的版本
    uniapp_offline_sdk_version: str  # uniapp离线sdk版本
    project_appid: str  # 项目appID
    project_appkey: str  # 项目appkey
    project_package_name: str  # app 包名
    project_pack_name: str  # app 应用名称
    project_version_code: str  # app 版本号
    project_version_name: str  # app 版本名称
    keystore_file_path: str  # 签名文件路径
    keystore_alias: str  # 签名文件别名
    keystore_password: str  # 签名文件密码
    keystore_storePassword: str  # 签名文件store密码
    permissions: str  # app 权限菜单
    log_path: str  # app log文件夹地址

    def __init__(self, project_name: str = '',
                 project_save_name: str = '',
                 project_path: str = '',
                 android_sdk_home: str = '',
                 java_version: str = '',
                 gradle_version: str = '',
                 hbuilderx_version: str = '',
                 uniapp_offline_sdk_version: str = '',
                 project_appid: str = '',
                 project_appkey: str = '',
                 project_package_name: str = '',
                 project_pack_name: str = '',
                 project_version_code: str = '',
                 project_version_name: str = '',
                 keystore_file_path: str = '',
                 keystore_alias: str = '',
                 keystore_password: str = '',
                 keystore_storePassword: str = '',
                 permissions: str = '',
                 log_path: str = ''
                 ) -> None:
        self.project_name = project_name
        self.project_save_name = project_save_name
        self.project_path = project_path
        self.android_sdk_home = android_sdk_home
        self.java_version = java_version
        self.gradle_version = gradle_version
        self.hbuilderx_version = hbuilderx_version
        self.uniapp_offline_sdk_version = uniapp_offline_sdk_version
        self.project_appid = project_appid
        self.project_appkey = project_appkey
        self.project_package_name = project_package_name
        self.project_pack_name = project_pack_name
        self.project_version_code = project_version_code
        self.project_version_name = project_version_name
        self.keystore_file_path = keystore_file_path
        self.keystore_alias = keystore_alias
        self.keystore_password = keystore_password
        self.keystore_storePassword = keystore_storePassword
        self.permissions = permissions
        self.log_path = log_path

    def to_json(self):
        # , ensure_ascii=False 表示不将非ASCII转化成Unicode编码
        return json.dumps(self.__dict__, default=lambda o: dataclasses.asdict(o), ensure_ascii=False)

    @classmethod
    def from_json(cls, json_str):
        d: Dict[str: Any] = json.loads(json_str)
        return cls(**d)


class CustomText(object):
    def __init__(self, st: ScrolledText):
        self.queue = Queue()
        self.text_area = st
        self.redirector_running = True

    def write(self, s: str):
        self.queue.put(s)

    def start_redirection(self):
        def process_queue():
            while self.redirector_running:
                while not self.queue.empty():
                    self.text_area.insert(tk.END, self.queue.get())
                    self.text_area.see(tk.END)
                self.text_area.update_idletasks()
                self.text_area.update()

        t = threading.Thread(target=process_queue)
        t.daemon = True
        t.start()

    def stop_redirection(self):
        self.redirector_running = False


win = tk.Tk()
win.geometry('750x650')
win.title('uniapp离线打包工具')

# 打印控制台输出
st_1 = ScrolledText(win, width=103, height=15)
st_1.grid(column=0, columnspan=4, rowspan=1, row=21)
console_ = CustomText(st_1)
console_.start_redirection()
# 重定向标准输出到 ScrolledText 中
sys.stdout.write = console_.write
sys.stderr.write = console_.write

# 安卓AndroidManifest.xml文件路径
android_manifest_path = '/simpleDemo/src/main/AndroidManifest.xml'
# 安卓打包后的app_name文件路径
android_package_app_name = '/simpleDemo/src/main/res/values/strings.xml'
# Gradle 打包的签名，版本配置文件 build.gradle
build_gradle_path = '/simpleDemo/build.gradle'
# 指定jdk配置文件 gradle.properties 设置参数 org.gradle.java.home=
build_gradle_jdk_setting_path = 'gradle.properties'
# 指定sdk配置文件 local.properties，没有则创建  设置参数 sdk.dir=
build_gradle_sdk_setting_path = 'local.properties'
# app资源包设置app_id  dcloud_control.xml
dcloud_control_app_id_path = '/simpleDemo/src/main/assets/data/dcloud_control.xml'
# app log图资源位置路径，在res目录下，文件夹包含drawable的目录，暂时制作drawable目录，
# drawable-hdpi, drawable-ldpi, drawable-mdpi, drawable-xhdpi, drawable-xxhdpi 目录暂时不做处理
android_log_path = '/simpleDemo/src/main/res/drawable'

# 安卓sdk目录
android_sdk_home_list = ['请选择']
# java目录
java_home_list = dict()
java_home_list['请选择'] = ''
# Gradle目录
gradle_home_list = dict()
gradle_home_list['请选择'] = ''
# HbuilderX安装目录
hbuilderx_home_list = dict()
hbuilderx_home_list['请选择'] = ''
# 项目配置信息
project_list = dict()
project_list['请选择'] = ''
# uniapp 离线sdk
uniapp_offline_sdk_home_list = dict()
uniapp_offline_sdk_home_list['请选择'] = ''
# uniapp本地离线安卓sdk
uniapp_offline_sdk_path = './resource/uniapp-offline-sdk'
# 已保存的项目信息
save_project_map: [str, ProjectSetting] = dict()

# 读取配置文件
with open('./resource/android-sdk/android-sdk', 'r') as f:
    for l in f.readlines():
        android_sdk_home_list.append(l.replace('\n', '').replace('\\', '\\\\').replace(':', '\\:'))
with open('./resource/gradle-versions/gradle-version', 'r') as f:
    for l in f.readlines():
        first_space = l.find(' ')
        gradle_home_list[l[0:first_space]] = l[first_space + 1:].replace('\n', '')
        # gradle_home_list.append({'version': l[0:first_space], 'path': l[first_space + 1:]})
with open('./resource/java-versions/java-version', 'r') as f:
    for l in f.readlines():
        first_space = l.find(' ')
        java_home_list[l[0:first_space]] = l[first_space + 1:].replace('\n', '').replace('\\', '\\\\').replace(':',
                                                                                                               '\\:')
        # java_home_list.append({'version': l[0:first_space], 'path': l[first_space + 1:]})
with open('./resource/hbuilderx-versions/hbuilderx-version', 'r') as f:
    for l in f.readlines():
        first_space = l.find(' ')
        hbuilderx_home_list[l[0:first_space]] = l[first_space + 1:].replace('\n', '')
        # hbuilderx_home_list.append({'version': l[0:first_space], 'path': l[first_space + 1:]})
# 读取uniapp离线sdk数据
for f in os.listdir(uniapp_offline_sdk_path):
    sdk_version_path = os.path.join(uniapp_offline_sdk_path, f)
    sdk_project_name = os.listdir(sdk_version_path)[0]
    uniapp_offline_sdk_home_list[f] = os.path.join(sdk_version_path, sdk_project_name)
# 读取保存的配置信息
with open('./project/settings', 'r', encoding='utf-8') as f:
    project_settings_str_list = f.readlines()
    if len(project_settings_str_list) > 0:
        # d: Dict[str, Any] = json.loads(f.readlines()[0])
        data = json.loads(project_settings_str_list[0])
        for x in data.keys():
            save_project_map[x] = ProjectSetting.from_json(data[x])
        # save_project_map = json.loads(project_settings_str_list[0])
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：{save_project_map = }')

# 执行命令
# 打包uniapp成本地app资源命令
hbuilderx_bin_path = ''
# gradle bin path
gralde_bin_path = ''
# uniapp项目路径
# project_path = 'D:\\project\\project\\app'
project_path = ''
# uniapp 项目名称
# project_name = 'my-tools-app-vue3'
project_name = ''
# 应用程序名称
project_pack_name = ''
# uniapp app_id
project_app_id = ''
# project_app_id = '__UNI__7EDAFE9'
# uniapp app_key
# project_app_key = '97872453c3e336ea080367abc562a5f5'
project_app_key = ''
# uniapp 包名
project_package_name = ''
# uniapp 版本号
project_version_code = 100
# uniapp 版本名称
project_version_name = "1.0.0"
project_keystore_keyAlias = ''
project_keystore_keyPassword = ''
project_keystore_storeFile = ''
project_keystore_storePassword = ''
# uniapp离线sdk版本
uniapp_offline_sdk_version = ''
# jdk地址
jdk_path = ''
# sdk地址
sdk_path = ''
# 权限配置 audio permission 声音 Camera permission 摄像头
permissions_list = ['Audio', 'Camera']
# app log图，请将logo图放入同一个文件夹下，名字分别为
log_img_path = ''

# 执行打包命令
uniapp_build_command = '"{hbuilderx_bin_name}/cli.exe" publish --platform APP --type appResource --project "{project_name}"' \
    .format(hbuilderx_bin_name=hbuilderx_bin_path, project_name=project_name)

# uniapp离线sdk 安卓项目目录
uniapp_offline_sdk_as_project_path = ''
# uniapp 打包后的资源目录
uniapp_package_assets_path = project_path + '/' + project_name + '/unpackage/resources'
# 安卓项目打包命令
as_package_cmd = '"{gralde_bin_name}/bin/gradle.bat" clean assemble'.format(gralde_bin_name=gralde_bin_path)
# 安卓项目保存路径
as_project_save_path = './project/package-project/' + project_name
# 安卓项目uniapp资源保存路径
app_assets_save_path = as_project_save_path + '/simpleDemo/src/main/assets/apps'
# AndroidManifest.xml 文件地址
android_manifest_abs_path = as_project_save_path + android_manifest_path
# 安卓打包后的app_name文件地址
android_package_app_name_abs_path = as_project_save_path + android_package_app_name
# Gradle配置文件地址
build_gradle_abs_path = as_project_save_path + build_gradle_path
# Gradle 设置jdk配置文件地址
build_gradle_jdk_setting_abs_path = as_project_save_path + '/' + build_gradle_jdk_setting_path
# Gradle 设置sdk配置文件地址
build_gradle_sdk_setting_abs_path = as_project_save_path + '/' + build_gradle_sdk_setting_path
# uniapp app 资源配置app_id
dcloud_control_app_id_abs_path = as_project_save_path + dcloud_control_app_id_path
# 安装项目启动logo保存路径
as_project_log_assets_path = os.path.abspath(as_project_save_path) + android_log_path


# 指定uniapp本地app资源打包命令
def execUniAppPackage() -> bool:
    # 执行命令
    # os.system(uniapp_build_command) # 不建议使用
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：uniapp打包当前目录：' + os.getcwd())
    unpackage_path = os.getcwd() + '/' + project_name + '/unpackage'
    if os.path.exists(unpackage_path):
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：删除uniapp项目下的unpackage目录:{unpackage_path}')
        shutil.rmtree(unpackage_path)
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：uniapp本地app资源打包命令：' + uniapp_build_command)
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：正在执行uniapp本地app资源打包命令...')
    # subprocess.Popen为即时输出命令行信息
    result = subprocess.Popen(uniapp_build_command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                              universal_newlines=True)
    # subprocess.run 为等待执行完后打印结果
    # result = subprocess.run(uniapp_build_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # 获取命令上输出内容
    # print(result.stdout.decode(encoding='gbk'))
    # 循环读取输出流，直到没有更多输出
    for line in result.stdout:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：', end='')  # end='' 防止额外添加换行符
    # 等待进程完成
    result.wait()
    # 如果命令出错，则打印错误日志
    if result.stderr:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：uniapp本地app资源打包失败...')
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：', result.stderr.decode(encoding='gbk'))
        return False
    # 如果命令的返回码为0，则说明执行成功
    if result.returncode == 0:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：uniapp本地app资源打包成功，返回码：', result.returncode)
    return True


# 复制uniapp离线sdk中的已配置好的安卓项目文件
def copyOfflineSdkProject() -> bool:
    # 复制uniapp离线Android sdk的安卓项目到project/package-project目录下
    global uniapp_offline_sdk_as_project_path
    print(
        f'\n【-- {datetime.datetime.now().isoformat()} --】：正在复制uniapp 离线sdk的simpleDemo 安卓项目文件到./project/package-project目录下...')
    # uniapp_offline_sdk_path_list = os.listdir(uniapp_offline_sdk_path)
    # for name in uniapp_offline_sdk_path_list:
    #     print(f'\n【-- {datetime.datetime.now().isoformat()} --】：{name = }')
    #     if name == uniapp_offline_sdk_version:
    #         version_path = os.path.join(uniapp_offline_sdk_path, name)
    #         sdk_project_name = os.listdir(version_path)[0]
    #         uniapp_offline_sdk_as_project_path = os.path.join(version_path, sdk_project_name)
    # if uniapp_offline_sdk_path != '':
    #     if os.path.exists(as_project_save_path):
    #         # 移除已存在的目录和文件
    #         print(f'\n【-- {datetime.datetime.now().isoformat()} --】：{as_project_save_path}已存在，正在删除...')
    #         shutil.rmtree(as_project_save_path)
    #         print(f'\n【-- {datetime.datetime.now().isoformat()} --】：已删除{as_project_save_path}')
    #     print(f'\n【-- {datetime.datetime.now().isoformat()} --】：复制中...')
    #     print(f'\n【-- {datetime.datetime.now().isoformat()} --】：复制 {uniapp_offline_sdk_as_project_path} 至 {as_project_save_path}')
    #     # os.mkdir(as_project_save_path + "/" + project_name)  # 不需要创建目录
    #     shutil.copytree(uniapp_offline_sdk_as_project_path, as_project_save_path)
    # else:
    #     print(f'\n【-- {datetime.datetime.now().isoformat()} --】：未找到uniapp离线sdk的simpleDemo安卓项目文件，请确定文件已存在！')
    #     # 如果文件不存在，则直接退出
    #     return False
    #     # exit()
    # return True
    uniapp_offline_sdk_as_project_path = uniapp_offline_sdk_home_list[uniapp_offline_sdk_version]
    if uniapp_offline_sdk_version != '' and uniapp_offline_sdk_as_project_path != '':
        if os.path.exists(as_project_save_path):
            # 移除已存在的目录和文件
            print(f'\n【-- {datetime.datetime.now().isoformat()} --】：{as_project_save_path}已存在，正在删除...')
            shutil.rmtree(as_project_save_path)
            print(f'\n【-- {datetime.datetime.now().isoformat()} --】：已删除{as_project_save_path}')
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：复制中...')
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：当前目录：{os.getcwd()}')
        print(
            f'\n【-- {datetime.datetime.now().isoformat()} --】：复制 {uniapp_offline_sdk_as_project_path} 至 {as_project_save_path}')
        # os.mkdir(as_project_save_path + "/" + project_name)  # 不需要创建目录
        shutil.copytree(uniapp_offline_sdk_as_project_path, as_project_save_path)
    else:
        print(
            f'\n【-- {datetime.datetime.now().isoformat()} --】：未找到uniapp离线sdk的simpleDemo安卓项目文件，请确定文件已存在！')
        # 如果文件不存在，则直接退出
        return False
        # exit()
    return True


# 复制uniapp打包好的app本地资源
def copyUniappLocalAppResource() -> bool:
    # 复制uniapp打包好的app本地资源到 as_project_save_path 下
    if os.path.exists(app_assets_save_path):
        # 移除已存在的目录和文件
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：{app_assets_save_path}已存在，正在删除...')
        shutil.rmtree(app_assets_save_path)
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：已删除{app_assets_save_path}')
    print(
        f'\n【-- {datetime.datetime.now().isoformat()} --】：复制app本地资源 {uniapp_package_assets_path} 到 {app_assets_save_path}')
    # os.mkdir(as_project_save_path + "/" + project_name)  # 不需要创建目录
    shutil.copytree(uniapp_package_assets_path, app_assets_save_path)
    return True


# 配置appkey信息
def settingAppKey() -> bool:
    # 配置appkey信息
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：读取AndroidManifest.xml中，地址：{android_manifest_abs_path}')
    ET.register_namespace("android", "http://schemas.android.com/apk/res/android")
    manifest_xml = ET.parse(android_manifest_abs_path)
    manifest_root = manifest_xml.getroot()
    app_key_set_flag = False
    for application in manifest_root.find('application'):
        if application.tag == 'meta-data':
            if application.get('{http://schemas.android.com/apk/res/android}name') == 'dcloud_appkey':
                application.set('{http://schemas.android.com/apk/res/android}value', project_app_key)
                app_key_set_flag = True
    if app_key_set_flag:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：已成功设置AndroidManifest.xml的dcloud_appkey值')
        manifest_xml.write(android_manifest_abs_path, encoding='utf-8', xml_declaration=True)
    else:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：设置AndroidManifest.xml的dcloud_appkey值失败')
        # exit()
        return False
    return True


# 配置app的名称
def settingAppName() -> bool:
    # 配置app的名称
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：读取strings.xml中，地址：{android_package_app_name_abs_path}')
    app_name_xml = ET.parse(android_package_app_name_abs_path)
    app_name_root = app_name_xml.getroot()
    app_name_set_flag = False
    for application in app_name_root.findall('string'):
        if application.get('name') == 'app_name':
            app_name_set_flag = True
            # application.text = project_name
            application.text = project_pack_name
    if app_name_set_flag:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：已成功设置strings.xml的app_name值')
        app_name_xml.write(android_package_app_name_abs_path, encoding='utf-8', xml_declaration=True)
    else:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：设置strings.xml的app_name值失败')
        # exit()
        return False
    return True


# 配置app 资源的app_id信息
def settingAppId() -> bool:
    # 配置app 资源的app_id信息
    print(
        f'\n【-- {datetime.datetime.now().isoformat()} --】：读取dcloud_control.xml中，地址：{dcloud_control_app_id_abs_path}')
    dcloud_control_xml = ET.parse(dcloud_control_app_id_abs_path)
    dcloud_control_root = dcloud_control_xml.getroot()
    app_id_set_flag = False
    for application in dcloud_control_root.find('apps'):
        if application.tag == 'app':
            if application.get('appid') == '__UNI__A':
                application.set('appid', project_app_id)
                app_id_set_flag = True
    if app_id_set_flag:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：已成功设置dcloud_control.xml的appid值')
        dcloud_control_xml.write(dcloud_control_app_id_abs_path, encoding='utf-8', xml_declaration=True)
    else:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：设置AndroidManifest.xml的dcloud_appkey值失败')
        # exit()
        return False
    return True


# 设置jdk配置
def settingGradleJdkHome() -> bool:
    # 设置jdk配置
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：正在设置Gradle打包的jdk路径...')
    print(
        f'\n【-- {datetime.datetime.now().isoformat()} --】：设置Gradle打包的jdk文件路径：{build_gradle_jdk_setting_abs_path}')
    jdk_lines = None
    jdk_set_flag = False
    with open(build_gradle_jdk_setting_abs_path, 'r') as f:
        jdk_lines = f.readlines()
    with open(build_gradle_jdk_setting_abs_path, 'w') as w:
        for l in jdk_lines:
            if 'org.gradle.java.home=' in l:
                w.write(f'org.gradle.java.home={jdk_path}')
                jdk_set_flag = True
            else:
                w.write(l)
            w.write('\n')
        if jdk_set_flag is False:
            w.write(f'org.gradle.java.home={jdk_path}')
    return True


# 设置sdk配置
def settingGradleSdkHome() -> bool:
    # 设置sdk配置
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：正在设置Gradle打包的sdk路径...')
    print(
        f'\n【-- {datetime.datetime.now().isoformat()} --】：设置Gradle打包的sdk文件路径：{build_gradle_sdk_setting_abs_path}')
    if os.path.exists(build_gradle_sdk_setting_abs_path):
        sdk_lines = None
        sdk_set_flag = False
        with open(build_gradle_jdk_setting_abs_path, 'r') as f:
            sdk_lines = f.readlines()
        with open(build_gradle_sdk_setting_abs_path, 'w') as w:
            for l in sdk_lines:
                if 'sdk.dir=' in l:
                    w.write(f'sdk.dir={sdk_path}')
                    sdk_set_flag = True
                else:
                    w.write(l)
                w.write('\n')
            if sdk_set_flag is False:
                w.write(f'sdk.dir={sdk_path}')
    else:
        with open(build_gradle_sdk_setting_abs_path, 'w') as f:
            f.write(f'sdk.dir={sdk_path}')
    return True


# 配置 build.gradle 信息
def settingGradleBuildInfo() -> bool:
    # 配置 build.gradle 信息
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：正在配置build.gradle文件（项目版本与签名信息）...')
    build_gradle_lines = None
    with open(build_gradle_abs_path, 'r') as f:
        build_gradle_lines = f.readlines()
    with open(build_gradle_abs_path, 'w') as w:
        for l in build_gradle_lines:
            if 'applicationId' in l:
                w.write(l.replace('com.android.simple', project_package_name))
            elif 'versionCode' in l:
                w.write(l.replace('1', str(project_version_code)))
            elif 'versionName' in l:
                w.write(l.replace('1.0', project_version_name))
            elif 'keyAlias' in l:
                w.write(l.replace('key0', project_keystore_keyAlias))
            elif 'keyPassword' in l:
                w.write(l.replace('123456', project_keystore_keyPassword))
            elif 'storeFile file' in l:
                w.write(l.replace('test.jks', project_keystore_storeFile))
            elif 'storePassword' in l:
                w.write(l.replace('123456', project_keystore_storePassword))
            else:
                w.write(l)
    return True


# 配置应用的app权限
def settingAppPermission() -> bool:
    # 配置appkey信息
    print(
        f'\n【-- {datetime.datetime.now().isoformat()} --】：配置app权限中，读取AndroidManifest.xml中，地址：{android_manifest_abs_path}')
    ET.register_namespace("android", "http://schemas.android.com/apk/res/android")
    manifest_xml = ET.parse(android_manifest_abs_path)
    manifest_root = manifest_xml.getroot()
    if len(permissions_list) <= 0:
        return True
    app_key_set_flag = False
    for p in permissions_list:
        if p == 'Audio':
            permission_el = ET.Element('uses-permission')
            permission_el.set('{http://schemas.android.com/apk/res/android}name', 'android.permission.RECORD_AUDIO')
            manifest_root.append(permission_el)
            permission_el = ET.Element('uses-permission')
            permission_el.set('{http://schemas.android.com/apk/res/android}name',
                              'android.permission.MODIFY_AUDIO_SETTINGS')
            manifest_root.append(permission_el)
            app_key_set_flag = True
        elif p == 'Camera':
            permission_el = ET.Element('uses-permission')
            permission_el.set('{http://schemas.android.com/apk/res/android}name', 'android.permission.CAMERA')
            manifest_root.append(permission_el)
            permission_el = ET.Element('uses-permission')
            permission_el.set('{http://schemas.android.com/apk/res/android}name', 'android.hardware.camera')
            manifest_root.append(permission_el)
            app_key_set_flag = True
    if app_key_set_flag:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：已成功设置app的权限，AndroidManifest.xml的permission值')
        manifest_xml.write(android_manifest_abs_path, encoding='utf-8', xml_declaration=True)
    else:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：设置app的权限，AndroidManifest.xml的permission值失败')
        # exit()
        return False
    return True


# 配置应用的app logo图
def settingAppLogImage() -> bool:
    # 配置应用的app logo图信息
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：配置app logo图中...')
    if log_img_path == '' or log_img_path is None:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：未配置app logo图，使用默认UniApp logo...')
        return True
    # 需要复制的logo图片 icon.png,push.png,splash.png
    logs_name_list = ['icon', 'push', 'splash']
    resource_logs = os.listdir(log_img_path)
    need_copy_file_name_logs = [x for x in resource_logs if x.split('.')[0] in logs_name_list]
    if len(need_copy_file_name_logs) <= 0:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：未配置app logo图，使用默认UniApp logo...')
        return True
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：删除默认UniApp logo...')
    shutil.rmtree(as_project_log_assets_path)
    if os.path.exists(as_project_log_assets_path) is False:
        os.mkdir(as_project_log_assets_path)
    for f_name in need_copy_file_name_logs:
        # 只复制文件名为'icon', 'push', 'splash'，其他的忽略
        if f_name.split('.')[0] in logs_name_list:
            dest_file_path = os.path.join(as_project_log_assets_path, f_name)
            src_file_path = os.path.join(log_img_path, f_name)
            print(f'\n【-- {datetime.datetime.now().isoformat()} --】：复制文件【{f_name}】至【{dest_file_path}】下')
            shutil.copy(src_file_path, dest_file_path)
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：已配置app logo图...')
    return True


# 打包安卓项目
def asProjectPackage() -> bool:
    # 打包安卓项目
    # 执行命令
    # 获取当前路径的绝对路径，以用于返回当前目录
    # python_exec_path = os.getcwd()
    # os.system(uniapp_build_command) # 不建议使用
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：安卓打包命令：' + as_package_cmd)
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：正在执行安卓打包命令...')
    # subprocess.Popen为即时输出命令行信息
    result = subprocess.Popen(as_package_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
    pack_flag = True
    # 循环读取输出流，直到没有更多输出
    for line in result.stdout:
        print(line, end='')  # end='' 防止额外添加换行符
        # 如果打包过程出现了 'BUILD FAILED' 字样，表示打包失败
        if 'BUILD FAILED' in line:
            pack_flag = False
    # 等待进程完成
    result.wait()
    # 如果命令出错，则打印错误日志
    if result.stderr or pack_flag is False:
        if pack_flag is False:
            print(f'\n【-- {datetime.datetime.now().isoformat()} --】：安卓打包失败...')
        else:
            print(result.stderr.decode(encoding='gbk'))
        return False
    # 如果命令的返回码为0，则说明执行成功
    if result.returncode == 0:
        print(
            f'安卓打包成功，文件目录：{os.getcwd() + "/simpleDemo/build/outputs/apk/release"}，返回码：{result.returncode}')
        apk_save_path = os.getcwd() + '/simpleDemo/build/outputs/apk/release'
        apk_file_name = 'simpleDemo-release.apk'
        shutil.copyfile(apk_save_path + '/' + apk_file_name, apk_save_path + '/' + project_pack_name + '.apk')
        zip_file(apk_save_path + '/' + project_pack_name + '.apk', project_pack_name + '.apk',
                 apk_save_path + '/' + project_pack_name + '.zip')
    return True


# 压缩文件夹
def zip_folder(folder_path, output_path):
    with zipfile.ZipFile(output_path, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for root, dirs, files in os.walk(folder_path):
            for file in files:
                file_path = os.path.join(root, file)
                file_in_zip_path = os.path.relpath(file_path, os.path.dirname(folder_path))
                zipf.write(file_path, file_in_zip_path)


# 压缩文件
def zip_file(file_path, file_name, output_file_path):
    with zipfile.ZipFile(output_file_path, 'w') as zipf:
        zipf.write(file_path, arcname=file_name, compress_type=zipfile.ZIP_DEFLATED)


# 保存配置文件
def saveAllSettings():
    setParams()
    global save_project_map
    with open('./project/settings', 'w', encoding='utf-8') as f:
        if save_project_map is not None and len(save_project_map.keys()) > 0:
            s: [str, str] = dict()
            for k in save_project_map.keys():
                if isinstance(save_project_map[k], ProjectSetting):
                    s[k] = save_project_map[k].to_json()
                else:
                    s[k] = save_project_map[k]
            # ensure_ascii=False 表示转换时，不将非ASCII转化成Unicode编码
            f.write(json.dumps(s, ensure_ascii=False))
            # f.write(json.dumps(a, default=lambda o: asdict(save_project_map)))
        else:
            f.write('')


# 保存当前项目配置
def saveCurrentSetting():
    sd = simpledialog.askstring('保存', '请输入配置名称')
    setParams()
    cu = ProjectSetting()
    cu.project_name = project_name
    if cu.project_name == '':
        messagebox.showinfo('提醒', '保存文件，未输入项目名称')
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：保存文件，未输入项目名称')
        return
    if sd != '':
        cu.project_save_name = sd
    if cu.project_save_name == '':
        cu.project_save_name = cu.project_name + '_' + datetime.date.today().isoformat()
    if cu.project_save_name in save_project_map.keys():
        messagebox.showinfo('提醒', '配置名称已存在，请重新输入')
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：配置名称已存在，请重新输入')
        return
    cu.project_path = project_path
    cu.android_sdk_home = sdk_path
    cu.java_version = java_home_list_box.get()
    cu.gradle_version = gradle_home_list_box.get()
    cu.hbuilderx_version = hbuilderx_home_list_box.get()
    cu.uniapp_offline_sdk_version = uniapp_offline_sdk_list_box.get()
    cu.project_appid = project_app_id
    cu.project_appkey = project_app_key
    cu.project_package_name = project_package_name
    cu.project_pack_name = project_pack_name
    cu.project_version_code = project_version_code
    cu.project_version_name = project_version_name
    cu.keystore_file_path = project_keystore_storeFile
    cu.keystore_alias = project_keystore_keyAlias
    cu.keystore_password = project_keystore_keyPassword
    cu.keystore_storePassword = project_keystore_storePassword
    cu.permissions = permissions_list
    cu.log_path = log_img_path
    save_project_map[cu.project_save_name] = cu
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：保存配置成功，配置名称为：{cu.project_save_name}')
    # 将保存选择框更新
    s_lb_1.delete(0, len(save_project_map.keys()) - 1)
    for i_, item_ in enumerate(save_project_map.keys()):
        s_lb_1.insert(i_, item_)
    # 将系统配置文件保存到文件中
    saveAllSettings()


# 执行所有命令操作
def execPackage() -> bool:
    # mb = messagebox.showinfo('提醒', '打包中')
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：======================开始打包==================')
    setParams()
    # 获取当前路径的绝对路径，以用于返回当前目录
    python_exec_path = os.getcwd()
    try:
        # 切换工作目录至uniapp项目目录下
        os.chdir(project_path + '/' + project_name)
        if execUniAppPackage() is False:
            return False
    except Exception:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：执行uniapp打包异常...')
        traceback.print_exc()
        return False
    finally:
        # 切换回原有目录
        os.chdir(python_exec_path)
    try:
        if copyOfflineSdkProject() is False:
            return False
    except Exception:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：执行复制uniapp离线sdk中的已配置好的安卓项目文件异常...')
        traceback.print_exc()
        return False
    try:
        if copyUniappLocalAppResource() is False:
            return False
    except Exception:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：执行复制uniapp打包好的app本地资源异常...')
        traceback.print_exc()
        return False
    try:
        if settingAppKey() is False:
            return False
    except Exception:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：执行配置appkey信息异常...')
        traceback.print_exc()
        return False
    try:
        if settingAppName() is False:
            return False
    except Exception:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：执行配置app的名称异常...')
        traceback.print_exc()
        return False
    try:
        if settingAppId() is False:
            return False
    except Exception:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：执行配置app资源的app_id信息异常...')
        traceback.print_exc()
        return False
    try:
        if settingGradleJdkHome() is False:
            return False
    except Exception:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：执行设置jdk配置异常...')
        traceback.print_exc()
        return False
    try:
        if settingGradleSdkHome() is False:
            return False
    except Exception:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：执行设置sdk配置异常...')
        traceback.print_exc()
        return False
    try:
        if settingGradleBuildInfo() is False:
            return False
    except Exception:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：执行配置 build.gradle 信息异常...')
        traceback.print_exc()
        return False
    try:
        if settingAppPermission() is False:
            return False
    except Exception:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：设置app权限异常...')
        traceback.print_exc()
        return False
    try:
        if settingAppLogImage() is False:
            return False
    except Exception:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：设置app logo异常...')
        traceback.print_exc()
        return False
    try:
        # 切换工作目录至uniapp项目目录下
        os.chdir(as_project_save_path)
        if asProjectPackage() is False:
            return False
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：======================打包成功==================')
    except Exception:
        print(f'\n【-- {datetime.datetime.now().isoformat()} --】：执行打包安卓项目异常...')
        traceback.print_exc()
        return False
    finally:
        os.chdir(python_exec_path)


# 异步启动打包
def asyncExecPackage() -> bool:
    thread1 = threading.Thread(target=execPackage)
    thread1.start()
    return True


def android_sdk_home_list_select(event):
    global sdk_path
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：{android_sdk_home_list_box.current()}')
    # sdk_path = android_sdk_home_list[int(android_sdk_home_list_box.get())].replace('\\', '\\\\').replace(':', '\\:')
    sdk_path = android_sdk_home_list_box.get()
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：{android_sdk_home_list_box.get(), sdk_path}')


def java_home_list_select(event):
    global jdk_path
    # jdk_path = list(java_home_list.values())[int(java_home_list_box.get())].replace('\\', '\\\\').replace(':', '\\:')
    jdk_path = java_home_list[java_home_list_box.get()]
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：{java_home_list_box.get(), jdk_path}')


def gradle_home_list_select(event):
    global gralde_bin_path
    # gralde_bin_path = list(gradle_home_list.values())[int(gradle_home_list_box.get())]
    gralde_bin_path = gradle_home_list[gradle_home_list_box.get()]
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：{gradle_home_list_box.get(), gralde_bin_path}')


def hbuilderx_home_list_select(event):
    global hbuilderx_bin_path
    # hbuilderx_bin_path = list(hbuilderx_home_list.values())[int(hbuilderx_home_list_box.get())]
    hbuilderx_bin_path = hbuilderx_home_list[hbuilderx_home_list_box.get()]
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：{hbuilderx_home_list_box.get(), hbuilderx_bin_path}')


def uniapp_offline_sdk_list_select(event):
    global uniapp_offline_sdk_version
    # uniapp_offline_sdk_path = list(uniapp_offline_sdk_home_list.values())[int(uniapp_offline_sdk_list_box.get())]
    uniapp_offline_sdk_version = uniapp_offline_sdk_list_box.get()
    print(
        f'\n【-- {datetime.datetime.now().isoformat()} --】：{uniapp_offline_sdk_list_box.get(), uniapp_offline_sdk_path}')


def android_sdk_manage_click(event):
    pass


def java_home_manage_click(event):
    pass


def gradle_home_manage_click(event):
    pass


def hbuilderx_home_manage_click(event):
    pass


def uniapp_offline_sdk_manage_click(event):
    pass


# 文件选择器
def file_select_btn_click():
    global project_path
    project_path = filedialog.askdirectory(title='选择项目目录', initialdir='/')
    l7_1.config(text=project_path)


# 签名文件选择器
def sign_file_select_btn_click():
    global project_keystore_storeFile
    project_keystore_storeFile = filedialog.askopenfilename(title='选择签名文件', initialdir='/',
                                                            filetypes=[('keystore', '*.keystore')]).replace('\\', '\\\\')
    l15_2.delete(0, len(l15_2.get()))
    l15_2.insert(0, project_keystore_storeFile)


# logo文件选择器
def logo_file_select_btn_click():
    global log_img_path
    log_img_path = filedialog.askdirectory(title='选择项目目录', initialdir='/')
    l16_1.config(text=project_path)


# 初始化参数
def initParams():
    global uniapp_build_command, \
        uniapp_offline_sdk_as_project_path, \
        uniapp_package_assets_path, \
        as_package_cmd, \
        as_project_save_path, \
        app_assets_save_path, \
        android_manifest_abs_path, \
        android_package_app_name_abs_path, \
        build_gradle_abs_path, \
        build_gradle_jdk_setting_abs_path, \
        build_gradle_sdk_setting_abs_path, \
        dcloud_control_app_id_abs_path, \
        as_project_log_assets_path
    # 执行打包命令
    uniapp_build_command = '"{hbuilderx_bin_name}/cli.exe" publish --platform APP --type appResource --project "{project_name}"' \
        .format(hbuilderx_bin_name=hbuilderx_bin_path, project_name=project_name)

    # uniapp离线sdk 安卓项目目录
    uniapp_offline_sdk_as_project_path = ''
    # uniapp 打包后的资源目录
    uniapp_package_assets_path = project_path + '/' + project_name + '/unpackage/resources'
    # 安卓项目打包命令
    as_package_cmd = '"{gralde_bin_name}/bin/gradle.bat" clean assemble'.format(gralde_bin_name=gralde_bin_path)
    # 安卓项目保存路径
    as_project_save_path = './project/package-project/' + project_name
    # 安卓项目uniapp资源保存路径
    app_assets_save_path = as_project_save_path + '/simpleDemo/src/main/assets/apps'
    # AndroidManifest.xml 文件地址
    android_manifest_abs_path = as_project_save_path + android_manifest_path
    # 安卓打包后的app_name文件地址
    android_package_app_name_abs_path = as_project_save_path + android_package_app_name
    # Gradle配置文件地址
    build_gradle_abs_path = as_project_save_path + build_gradle_path
    # Gradle 设置jdk配置文件地址
    build_gradle_jdk_setting_abs_path = as_project_save_path + '/' + build_gradle_jdk_setting_path
    # Gradle 设置sdk配置文件地址
    build_gradle_sdk_setting_abs_path = as_project_save_path + '/' + build_gradle_sdk_setting_path
    # uniapp app 资源配置app_id
    dcloud_control_app_id_abs_path = as_project_save_path + dcloud_control_app_id_path
    # 安装项目启动logo保存路径
    as_project_log_assets_path = os.path.abspath(as_project_save_path) + android_log_path


# 将组件里的数据设置到全局参数中
def setParams():
    global project_name, project_pack_name, project_app_id, project_app_key, project_package_name, project_version_code
    global project_version_name, project_keystore_keyAlias, project_keystore_keyPassword, project_keystore_keyPassword
    global project_keystore_storePassword, permissions_list
    project_name = e8.get()
    project_app_id = e10_1.get()
    project_app_key = e10_2.get()
    project_package_name = e11_1.get()
    project_pack_name = e11_2.get()
    project_version_code = e12_1.get()
    project_version_name = e12_2.get()
    project_keystore_keyAlias = e14_1.get()
    project_keystore_keyPassword = e14_2.get()
    project_keystore_storePassword = e15_1.get()
    # 初始化参数
    initParams()


# 构建选择框相关数据
def buildSelect(win_: tk.Tk, values: list[str],
                func: Callable, c: int, cs: int, r: int, rs: int) -> (Combobox, tk.IntVar):
    number = tk.IntVar()
    cb = ttk.Combobox(win_, width=30, height=4, textvariable=number)
    if len(values) <= 0:
        values = ['请选择']
    cb['value'] = values
    cb.current(0)
    cb.bind("<<ComboboxSelected>>", func)
    cb.grid(column=c, columnspan=cs, row=r, rowspan=rs)
    return cb, number


# 使用项目配置
def applySetting():
    global sdk_path, \
        hbuilderx_bin_path, \
        gralde_bin_path, \
        uniapp_offline_sdk_version, \
        jdk_path, \
        project_path, \
        project_name, \
        project_app_id, \
        project_app_key, \
        project_package_name, \
        project_pack_name, \
        project_version_code, \
        project_version_name, \
        project_keystore_keyAlias, \
        project_keystore_keyPassword, \
        project_keystore_storePassword, \
        project_keystore_storeFile, \
        permissions_list
    delete_index = s_lb_1.curselection()[0]
    if delete_index is None:
        return
    ps = save_project_map[list(save_project_map.keys())[delete_index]]
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：应用了{s_lb_1.curselection()[0], ps}')
    sdk_path = ps.android_sdk_home
    hbuilderx_bin_path = hbuilderx_home_list[ps.hbuilderx_version]
    gralde_bin_path = gradle_home_list[ps.gradle_version]
    uniapp_offline_sdk_version = ps.uniapp_offline_sdk_version
    jdk_path = java_home_list[ps.java_version]
    project_path = ps.project_path
    project_name = ps.project_name
    project_app_id = ps.project_appid
    project_app_key = ps.project_appkey
    project_package_name = ps.project_package_name
    project_pack_name = ps.project_pack_name
    project_version_code = ps.project_version_code
    project_version_name = ps.project_version_name
    project_keystore_keyAlias = ps.keystore_alias
    project_keystore_keyPassword = ps.keystore_password
    project_keystore_storePassword = ps.keystore_storePassword
    project_keystore_storeFile = ps.keystore_file_path
    permissions_list = ps.permissions
    log_img_path = ps.log_path
    # android_sdk_home_list_box
    android_sdk_home_list_box.current(android_sdk_home_list.index(ps.android_sdk_home)
                                      if android_sdk_home_list.index(ps.android_sdk_home) >= 0 else 0)
    # java_home_list_box
    java_home_list_box.current(list(java_home_list.keys()).index(ps.java_version)
                               if list(java_home_list.keys()).index(ps.java_version) >= 0 else 0)
    # gradle_home_list_box
    gradle_home_list_box.current(list(gradle_home_list.keys()).index(ps.gradle_version)
                                 if list(gradle_home_list.keys()).index(ps.gradle_version) >= 0 else 0)
    # hbuilderx_home_list_box
    hbuilderx_home_list_box.current(list(hbuilderx_home_list.keys()).index(ps.hbuilderx_version)
                                    if list(hbuilderx_home_list.keys()).index(ps.hbuilderx_version) >= 0 else 0)
    # uniapp_offline_sdk_list_box
    uniapp_offline_sdk_list_box.current(list(uniapp_offline_sdk_home_list.keys()).index(ps.uniapp_offline_sdk_version)
                                        if list(uniapp_offline_sdk_home_list.keys()).index(
        ps.uniapp_offline_sdk_version) >= 0 else 0)
    l7_1.config(text=project_path)
    e8.delete(0, len(e8.get()))
    e10_1.delete(0, len(e10_1.get()))
    e10_2.delete(0, len(e10_2.get()))
    e11_1.delete(0, len(e11_1.get()))
    e11_2.delete(0, len(e11_2.get()))
    e12_1.delete(0, len(e12_1.get()))
    e12_2.delete(0, len(e12_2.get()))
    e14_1.delete(0, len(e14_1.get()))
    e14_2.delete(0, len(e14_2.get()))
    e15_1.delete(0, len(e15_1.get()))
    l15_2.delete(0, len(l15_2.get()))
    e8.insert(0, project_name)
    e10_1.insert(0, project_app_id)
    e10_2.insert(0, project_app_key)
    e11_1.insert(0, project_package_name)
    e11_2.insert(0, project_pack_name)
    e12_1.insert(0, project_version_code)
    e12_2.insert(0, project_version_name)
    e14_1.insert(0, project_keystore_keyAlias)
    e14_2.insert(0, project_keystore_keyPassword)
    e15_1.insert(0, project_keystore_storePassword)
    l15_2.insert(0, project_keystore_storeFile.replace('\\', '\\\\'))
    l16_1.config(text=log_img_path)
    # 初始化参数
    setParams()


# 删除项目配置
def deleteSetting():
    delete_index = s_lb_1.curselection()[0]
    if delete_index is None:
        return
    print(f'\n【-- {datetime.datetime.now().isoformat()} --】：删除了{s_lb_1.curselection()[0]}')
    save_project_map.__delitem__(list(save_project_map.keys())[delete_index])
    # 将保存选择框更新
    # s_lb_1.delete(0, len(save_project_map.keys()) - 1)
    s_lb_1.delete(0, len(save_project_map.keys()))
    for i_, item_ in enumerate(save_project_map.keys()):
        s_lb_1.insert(i_, item_)
    saveAllSettings()


# 查看apk文件
def openSuccessPackage():
    apk_path = os.getcwd() + '/project/package-project/' + project_name + '/simpleDemo/build/outputs/apk/release'
    print(f'================{apk_path = }')
    os.startfile(os.getcwd() + '/project/package-project/' + project_name + '/simpleDemo/build/outputs/apk/release')


# 下方为UI设计


# grid：网格形式，指定几行几列 pack：直接填入，有空白处就填 place：自行指定位置
label0 = ttk.Label(win, text='uniapp离线打包工具')
label0.grid(column=0, columnspan=4, row=0, rowspan=1)

label1 = ttk.Label(win, text='安卓sdk目录：')
label1.grid(column=0, columnspan=1, row=1, rowspan=1)

android_sdk_home_list_box, android_sdk_home_select_number = buildSelect(win,
                                                                        android_sdk_home_list,
                                                                        android_sdk_home_list_select,
                                                                        1, 1, 1, 1)

bt1 = ttk.Button(win, text='android sdk管理', width=20)
bt1.grid(column=2, columnspan=1, row=1, rowspan=1)

label2 = ttk.Label(win, text='Java版本：')
label2.grid(column=0, columnspan=1, row=2, rowspan=1)

java_home_list_box, java_home_select_number = buildSelect(win,
                                                          list(java_home_list.keys()),
                                                          java_home_list_select,
                                                          1, 1, 2, 1)

bt2 = ttk.Button(win, text='Java 管理', width=20)
bt2.grid(column=2, columnspan=1, row=2, rowspan=1)

label3 = ttk.Label(win, text='gradle版本：')
label3.grid(column=0, columnspan=1, row=3, rowspan=1)

gradle_home_list_box, gradle_home_select_number = buildSelect(win,
                                                              list(gradle_home_list.keys()),
                                                              gradle_home_list_select,
                                                              1, 1, 3, 1)

bt3 = ttk.Button(win, text='gradle 管理', width=20)
bt3.grid(column=2, columnspan=1, row=3, rowspan=1)

label4 = ttk.Label(win, text='hbuilderx版本：')
label4.grid(column=0, columnspan=1, row=4, rowspan=1)

hbuilderx_home_list_box, hbuilderx_home_select_number = buildSelect(win,
                                                                    list(hbuilderx_home_list.keys()),
                                                                    hbuilderx_home_list_select,
                                                                    1, 1, 4, 1)

bt4 = ttk.Button(win, text='HbuilderX 管理', width=20)
bt4.grid(column=2, columnspan=1, row=4, rowspan=1)

label5 = ttk.Label(win, text='uniapp离线sdk版本：')
label5.grid(column=0, columnspan=1, row=5, rowspan=1)

uniapp_offline_sdk_list_box, uniapp_offline_sdk_select_number = buildSelect(win,
                                                                            list(uniapp_offline_sdk_home_list.keys()),
                                                                            uniapp_offline_sdk_list_select,
                                                                            1, 1, 5, 1)

bt4 = ttk.Button(win, text='uniapp 离线sdk管理', width=20)
bt4.grid(column=2, columnspan=1, row=5, rowspan=1)

# 项目配置相关
l6 = ttk.Label(win, text='项目配置')
l6.grid(column=0, columnspan=4, row=6, rowspan=1)
l7 = ttk.Label(win, text='项目目录')
l7.grid(column=0, columnspan=1, row=7, rowspan=1)
l7_1 = ttk.Label(win, text=project_path)
l7_1.grid(column=1, columnspan=1, row=7, rowspan=1)
bt7 = ttk.Button(win, text='选择目录', width=20, command=file_select_btn_click)
bt7.grid(column=2, columnspan=1, row=7, rowspan=1)
l8 = ttk.Label(win, text='项目名称')
l8.grid(column=0, columnspan=1, row=8, rowspan=1)
e8 = ttk.Entry(win, width=32)
e8.grid(column=1, columnspan=1, row=8, rowspan=1)

# app配置信息
l9 = ttk.Label(win, text='app配置')
l9.grid(column=0, columnspan=4, row=9, rowspan=1)
l10_1 = ttk.Label(win, text='app id')
l10_1.grid(column=0, columnspan=1, row=10, rowspan=1)
e10_1 = ttk.Entry(win, width=32)
e10_1.grid(column=1, columnspan=1, row=10, rowspan=1)
l10_2 = ttk.Label(win, text='app key')
l10_2.grid(column=2, columnspan=1, row=10, rowspan=1)
e10_2 = ttk.Entry(win, width=32)
e10_2.grid(column=3, columnspan=1, row=10, rowspan=1)
l11_1 = ttk.Label(win, text='app 包名')
l11_1.grid(column=0, columnspan=1, row=11, rowspan=1)
e11_1 = ttk.Entry(win, width=32)
e11_1.grid(column=1, columnspan=1, row=11, rowspan=1)
l11_2 = ttk.Label(win, text='应用名称')
l11_2.grid(column=2, columnspan=1, row=11, rowspan=1)
e11_2 = ttk.Entry(win, width=32)
e11_2.grid(column=3, columnspan=1, row=11, rowspan=1)
l12_1 = ttk.Label(win, text='app 版本号')
l12_1.grid(column=0, columnspan=1, row=12, rowspan=1)
e12_1 = ttk.Entry(win, width=32)
e12_1.grid(column=1, columnspan=1, row=12, rowspan=1)
l12_2 = ttk.Label(win, text='app 版本名称')
l12_2.grid(column=2, columnspan=1, row=12, rowspan=1)
e12_2 = ttk.Entry(win, width=32)
e12_2.grid(column=3, columnspan=1, row=12, rowspan=1)

# app签名相关信息
l13 = ttk.Label(win, text='app签名配置')
l13.grid(column=0, columnspan=4, row=13, rowspan=1)
l14_1 = ttk.Label(win, text='签名Alias')
l14_1.grid(column=0, columnspan=1, row=14, rowspan=1)
e14_1 = ttk.Entry(win, width=32)
e14_1.grid(column=1, columnspan=1, row=14, rowspan=1)
l14_2 = ttk.Label(win, text='签名密码')
l14_2.grid(column=2, columnspan=1, row=14, rowspan=1)
e14_2 = ttk.Entry(win, width=32)
e14_2.grid(column=3, columnspan=1, row=14, rowspan=1)
l15_1 = ttk.Label(win, text='签名store密码')
l15_1.grid(column=0, columnspan=1, row=15, rowspan=1)
e15_1 = ttk.Entry(win, width=32)
e15_1.grid(column=1, columnspan=1, row=15, rowspan=1)
b15_2 = ttk.Button(win, text='签名文件', command=sign_file_select_btn_click)
b15_2.grid(column=2, columnspan=1, row=15, rowspan=1)
l15_2 = ttk.Entry(win, width=32)
l15_2.grid(column=3, columnspan=1, row=15, rowspan=1)

# app logo相关 TODO 后续再搞
l16_2 = ttk.Label(win, text='logo目录')
l16_2.grid(column=0, columnspan=1, row=16, rowspan=1)
l16_1 = ttk.Label(win, text='请选择logo目录')
l16_1.grid(column=1, columnspan=1, row=16, rowspan=1)
b16_1 = ttk.Button(win, width=20, text='选择logo目录', command=logo_file_select_btn_click)
b16_1.grid(column=2, columnspan=1, row=16, rowspan=1)

# 应用系统权限相关 permission等 TODO 后续再搞
# 项目配置使用相关
s_l_1 = ttk.Label(win, text='项目配置')
s_l_1.grid(row=0, rowspan=1, column=3, columnspan=1)
s_lb_1 = tk.Listbox(win, width=32, height=6)
s_lb_1.grid(row=1, rowspan=4, column=3, columnspan=1)
for i, item in enumerate(save_project_map.keys()):
    s_lb_1.insert(i, item)
# 获取值 s_lb_1.curselection()[0]
s_b_1 = ttk.Button(win, text='应用', width=20, command=applySetting)
s_b_1.grid(row=5, rowspan=1, column=3, columnspan=1)
s_b_2 = ttk.Button(win, text='删除', width=20, command=deleteSetting)
s_b_2.grid(row=6, rowspan=1, column=3, columnspan=1)

# begin_btn = ttk.Button(win, text='开始打包', width=20, command=execPackage)
begin_btn = ttk.Button(win, text='开始打包', width=20, command=asyncExecPackage)
begin_btn.grid(column=0, columnspan=2, rowspan=1, row=20)
save_setting_btn = ttk.Button(win, text='查看apk文件', width=20, command=openSuccessPackage)
save_setting_btn.grid(column=2, columnspan=1, rowspan=1, row=20)
save_setting_btn = ttk.Button(win, text='保存配置', width=20, command=saveCurrentSetting)
save_setting_btn.grid(column=3, columnspan=1, rowspan=1, row=20)

win.mainloop()
# 清理和节数打印
console_.stop_redirection()
